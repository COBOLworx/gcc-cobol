#!/usr/bin/python

# This one builds a simple cobol program with many variables, and a simple
# C program that parallels the data structure of those variables.  It was part
# of an effort to figure out why it takes the GCC middle end so long to just
# build the .S for the variables.

import sys

fout = None

def xprint(str):
  global fout
  # print(str)
  fout.write(str)
  fout.write("\n")

def cobol_code():
  global fout
  nlines = int(sys.argv[1])

  fout = open("test.cbl", "w")

  xprint("        identification division.")
  xprint("        program-id. nlines_cobol.")
  xprint("        data division.")
  xprint("        working-storage section.")

  for i in range(1, nlines+1):
    xprint("        77 var{0:d} pic X(24) value \"Hello, world! - {0:6d}\".".format(i))

  xprint("        procedure division.")

  for i in range(1, nlines+1):
    xprint("        display var{:d}".format(i))

  xprint("        stop run.")
  xprint("        end program nlines_cobol.")

  fout.close();

def c_code():
  global fout

  # To make the generation of C similar to the generation of COBOL, we add 14
  # variables to compensate for the COBOL boilerplate of 
  #   counter 1  DEBUG-ITEM
  #   counter 2  DEBUG-LINE
  #   counter 3  FILLER
  #   counter 4  DEBUG-NAME
  #   counter 5  FILLER
  #   counter 6  DEBUG-SUB-1
  #   counter 7  FILLER
  #   counter 8  DEBUG-SUB-2
  #   counter 9  FILLER
  #   counter 10 DEBUG-SUB-3
  #   counter 11 FILLER
  #   counter 12 DEBUG-CONTENTS
  #   counter 13 _literaln_1 "1"
  #   counter 14 _literaln_2 "0"
  nlines = int(sys.argv[1])

  fout = open("ccc.c", "w")

  xprint("#include <stdio.h>")
  xprint("#include <stdlib.h>")
  xprint("#include <string.h>")
  xprint("#include <unistd.h>")
  xprint(" ")

  # Establish the structures                                                                                     ")s
  xprint("typedef struct cblc_field_t                                                                              ")
  xprint("    {                                                                                                    ")
  xprint("    // This structure must match the code in structs.cc                                                  ")
  xprint("    unsigned char *data;              // The runtime data. There is no null terminator                   ")
  xprint("    unsigned long  capacity;          // The size of data                                                ")
  xprint("    unsigned long  allocated;         // The number of bytes available for capacity                      ")
  xprint("    unsigned long  offset;            // Offset from our ancestor (see note below)                       ")
  xprint("    char          *name;              // The null-terminated name of this variable                       ")
  xprint("    char          *picture;           // The null-terminated picture string.                             ")
  xprint("    char          *initial;           // The null_terminated initial value                               ")
  xprint("    struct cblc_field_t *parent;      // This field's immediate parent field                             ")
  xprint("    struct cblc_field_t *depending_on;// The subject of a DEPENDING ON clause                            ")
  xprint("    struct cblc_field_t *depends_on;  // Points downward in heirachy to the subordinate DEPENDING ON     ")
  xprint("    unsigned long occurs_lower;       // non-zero for a table                                            ")
  xprint("    unsigned long occurs_upper;       // non-zero for a table                                            ")
  xprint("    unsigned long attr;               // See cbl_field_attr_t                                            ")
  xprint("    signed char type;                 // A one-byte copy of cbl_field_type_t                             ")
  xprint("    signed char level;                // This variable's level in the naming heirarchy                   ")
  xprint("    signed char digits;               // Digits specified in PIC string; e.g. 5 for 99v999               ")
  xprint("    signed char rdigits;              // Digits to the right of the decimal point. 3 for 99v999          ")
  xprint("    int         dummy;                // Fills out to a four-byte boundary                               ")
  xprint("    } cblc_field_t;                                                                                      ")
  xprint(" ")
  xprint("typedef struct cblc_refer_t                                                                              ")
  xprint("    {                                                                                                    ")
  xprint("    // This structure must match the code in structs.cc                                                  ")
  xprint("    cblc_field_t     *field;                                                                             ")
  xprint("    unsigned char    *qual_data;   // As qualified by subscripts or refmods                              ")
  xprint("    size_t            qual_size;   // As qualified by refmods or depending_on                            ")
  xprint("    int               flags;       // combines all_flags, move_all, and address_of                       ")
  xprint("    int               dummy;       // Fill out to a multiple of eight bytes                              ")
  xprint("    } cblc_refer_t;                                                                                      ")
  xprint(" ")

  # Establish the initializer
  xprint("void                                                                                                     ")
  xprint("__gg__initialize_variable(cblc_refer_t *var_ref,                                                         ")
  xprint("                          int           is_redefined,                                                    ")
  xprint("                          unsigned int  nsubscripts,                                                     ")
  xprint("                          int           explicitly)                                                      ")
  xprint("  {                                                                                                      ")
  xprint("#if 0                                                                                                    ")
  xprint("  // This code emulates the call GCOBOL has to make to libgcobol.so in order                             ")
  xprint("  // to create and initialize a variable                                                                 ")
  xprint("  // It is based on an environment variable, which means that the                                        ")
  xprint("  // C optimizer can't easily just wish it away.                                                         ")
  xprint("  unsigned long retval;                                                                                  ")
  xprint("  char *p = getenv(\"banana\");")
  xprint("  int n = atoi(p ? p :\"0\");")
  xprint("  switch(n)                                                                                              ")
  xprint("    {                                                                                                    ")
  xprint("    case 1:                                                                                              ")
  xprint("      retval = (unsigned long)(&var_ref);                                                                ")
  xprint("      break;                                                                                             ")
  xprint("    case 2:                                                                                              ")
  xprint("      retval = is_redefined;                                                                             ")
  xprint("      break;                                                                                             ")
  xprint("    case 3:                                                                                              ")
  xprint("      retval = nsubscripts;                                                                              ")
  xprint("      break;                                                                                             ")
  xprint("    default:                                                                                             ")
  xprint("      retval = explicitly;                                                                               ")
  xprint("      break;                                                                                             ")
  xprint("    }                                                                                                    ")
  xprint("#endif                                                                                                   ")
  xprint("  }                                                                                                      ")
  xprint(" ")

  # Establish the principal routine()
  xprint("int nlines_c()")
  xprint("  {")
  xprint("  char *p = getenv(\"banana\");")
  xprint("  int i = atoi(p ? p :\"0\");")

  for i in range(1, nlines+14+1):
    xprint("  static unsigned char vardata{0:d}[24] = \"Hello, world! - {0:6d}\\n\";".format(i))

  for i in range(1, nlines+14+1):
    xprint("  static cblc_field_t var{0:d} = {{vardata{0:d}, 24, 24, 0, \"var{0:d}\", 0, 0, 0, 0, 0, 0, 0, 0, 6, 77, 8, 4}};".format(i))
    xprint("    if( !(var{0:d}.attr & 0x0200000000) )".format(i))
    xprint("      {")
    xprint("      int n = 0;")
    xprint("      cblc_refer_t refer_i{0:d};".format(i))
    xprint("        refer_i{0:d}.field = &var{0:d};".format(i))
    xprint("        refer_i{0:d}.flags = 0;".format(i))
    xprint("        refer_i{0:d}.qual_data = var{0:d}.data;".format(i))
    xprint("        refer_i{0:d}.qual_size = 24;".format(i))
    xprint("      __gg__initialize_variable(&refer_i{:d}, 1, 3, 4);".format(i))
    xprint("      }")

  for i in range(1, nlines+1):
    xprint("    cblc_refer_t refer{0:d};".format(i))
    xprint("      refer{0:d}.field = &var{0:d};".format(i))
    xprint("      refer{0:d}.flags = 0;".format(i))
    xprint("      refer{0:d}.qual_data = var{0:d}.data;".format(i))
    xprint("      refer{0:d}.qual_size = 24;".format(i))
    xprint("      write(1, refer{0:d}.qual_data, refer{0:d}.qual_size);".format(i))

  xprint("  }")

  xprint("")
  xprint("int main(){nlines_c();}")
  xprint("")

  fout.close();

cobol_code()
c_code()

