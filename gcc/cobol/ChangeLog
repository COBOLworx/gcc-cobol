2025-03-03  Robert Dubner <rdubner@symas.com>
	* Changed SECONDS-PAST-MIDNIGHT function to return nine digits of precision
	in accordance with the ISO spec of "as much precision as possible"

2025-02-25  Robert Dubner <rdubner@symas.com>
	* Added -m32 error message processing to gcobolspec.cc
	* Put charmaps-dupe.cc and valconv-dupe.cc into the repository
	* Modify cobol/Make-lang.in comments to reflect -dupe changes

2025-02-17  Robert Dubner <rdubner@symas.com>
	* Moved #include <math.h> from genapi.cc to cobol-system.h as
	#include <cmath>
	* Removed GCOBOL_FOR_TARGET from <repo>/Makefile.def
	* Removed if $USER = "bob" stuff from cobol/Make-lang.in
	* Backed -std=c++17 down to c++14 in cobol/Make-lang.in
	* Removed the single c++17 dependency from show_parse.h ANALYZER
	* Removed -Wno-cpp from cobol/Make-lang.in
	* Removed Wno-missing-field-initializers from cobol/Make-lang.in
	* Added some informative comments to placeholder functions in cobol1.cc
	* Removed a call to build_tree_list() in cobol1.cc
	* Use default for LANG_HOOKS_TYPE_FOR_SIZE in cobol1.cc
	* Commented out, but saved, unused code in convert.cc
	* Eliminated numerous "-Wmissing-field-initializers" warnings

2025-02-16  Robert Dubner <rdubner@symas.com>
	* Added GTY(()) tags to gengen.h and structs.h.  Put includes for them into
	cobol1.cc
	* Removed some fixed-length text buffers for handling mangled names

2025-02-11  Robert Dubner <rdubner@symas.com>
	* libgcobol quietly is not built for -m32 systems in a multi-lib build
	* configure.ac allows COBOL only for x86_64 and aarch64 architectures.
	Other systems get a warning and the COBOL language is suppressed.

2025-02-07  Robert Dubner <rdubner@symas.com>
	* Modified configure.ac and Makefile.in to notice that MULTISUBDIR=/32 to
	suppress 32-builds.
	* Eliminate -Wunused-result warning in libgcobol.cc compilation

2025-01-28  Robert Dubner <rdubner@symas.com>
	* Remove TRACE1 statements from parser_enter_file and parser_leave_file;
	they are incompatible with COPY statements in the DATA DIVISION.

2025-01-24  Robert Dubner <rdubner@symas.com>
	* Eliminated missing main() error message; we now rely on linker error
	* Cleaned up valconv-dupe and charmaps-dupe processing in Make-lang.in

2025-01-21  Robert Dubner <rdubner@symas.com>
	* Eliminated all "local" #includes from .h files; they are instead included,
	in order, in the .cc files.

2025-01-16  Robert Dubner <rdubner@symas.com>
	* Code 88 named-conditional comparisons for floating-point

2025-01-06  Robert Dubner <rdubner@symas.com>
	* Updated warning in tests/check_88 and etests/check_88
	* Updated some UAT error messages.

2025-01-03  Robert Dubner <rdubner@symas.com>
	* Eliminate old "#if 0" code
	* Modify line directives to skip over paragraph/section labels:
	* Unwrapped asprintf calls in assert(), because it was a stupid error.

2025-01-01  Robert Dubner <rdubner@symas.com>
	* Eliminate proc->target_of_call variable; it was unused.
	* Wrap asprintf calls in assert() to suppress compiler warnings.

2024-12-27  Robert Dubner <rdubner@symas.com>
	* Use built_in version of realloc and free
	* Use built_in version of strdup, memchr, and memset
	* Use built_in version of abort
	* Use built_in version of exit
	* Use built_in version of strncmp
	* Use built_in version of strcmp
	* Use built_in version of strcpy

2024-12-27  Robert Dubner <rdubner@symas.com>
	* Put called_by_main_counter in static memory, not the stack!

2024-12-26  Robert Dubner <rdubner@symas.com>
	* Use built_in version of memcpy
	* Use built_in version of malloc; required initialization
	during lang_hook_init

2024-12-25  Robert Dubner <rdubner@symas.com>
	* Normalize #includes in util.cc
	* Normalize #includes in symfind.cc
	* Normalize #includes in cdf-copy.cc and copybook.h
	* Normalize #includes in lexio.cc
	* Normalize #includes in cdf.y
	* Normalize #includes in scan.l
	required the creation of fisspace and fisdigit in util.cc
	* Normalize #includes in parse.y
	required the creation of ftolower in util.cc.  Jim uses things like
	std::transform, which can't take TOLOWER because it is a macro.  So I
	wrapped those necessary macros into functions.
	* Normalize #includes in symbols.h.cc

2024-12-23  Robert Dubner <rdubner@symas.com>

	* Created ChangeLog
	* Eliminate vestigial ".global" code
	* Create "cobol-system.h" file.
	trimmed .h files in cobol1.cc
	trimmed .h files in convert.cc
	trimmed .h files in except.cc
	trimmed .h files in gcobolspec.cc
	trimmed .h files in genapi.cc
	trimmed .h files in gengen.cc
	trimmed .h files in genmath.cc
	trimmed .h files in genutil.cc
	trimmed .h files in structs.cc

	* Eliminate vestigial unused code from the PERFORM-PROC-AS_CALL experiment
	* Fix TRACE1 problems with FldConditionals and parser_leave_file
	* Introduce cbl_message, cbl_warnx, cbl_error, cbl_internal_error
	convert genapi.cc to cbl_warnx and cbl_internal_error
	convert cdf-copy.cc to cbl_warnx
	convert cdf.y to cbl_warnx
	convert parse.y CDF display to cbl_message
	convert lexio.cc to cbl_warnx
	convert cobol1.cc to cbl_warnx
	convert copybook.h to cbl_warnx
	convert except.cc to cbl_internal_error and cbl_message
	convert genapi.cc to cbl_internal_error and cbl_message
	convert gengen.cc to cbl_internal_error and cbl_message
	convert genmath.cc to cbl_internal_error and cbl_message
	convert inspect.h to cbl_warnx
	convert lexio.cc to cbl_warnx
	convert parse.y cbl_warnx
	convert symfind.cc to cbl_warnx
	convert util.cc to cbl_warnx
	convert show-parse.h to cbl_warnx
	convert symbols.h to cbl_warnx
	convert symbols.cc to cbl_warnx
	fixed UAT for recursive copybook file warnings
	convert scan_ante.h to cbl_warnx
	convert scan_post.h to cbl_warnx
	convert scan.l to cbl_warnx
	convert parse_ante.h to cbl_warnx
	convert parse_util.h to cbl_warnx
	gcc/cobol has no calls to warnx
	gcc/cobol has no need for "err.h", and all calls have been converted to
	cbl_warn, cbl_warnx, cbl_err, cbl_errx

2024-12-22  Robert Dubner <rdubner@symas.com>

	* Reorganized libgcobol and gcc/cobol
	libgcobol now compiles independently of gcc/cobol
	gcc/cobol compiles using some .h and .cc files from gcc/libgcobol
^L
Copyright (C) 2022 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.
