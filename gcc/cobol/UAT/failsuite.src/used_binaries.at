## Copyright (C) 2014-2020 Free Software Foundation, Inc.
## Written by Simon Sobisch, Brian Tiffin
##
## This file is part of GnuCOBOL.
##
## The GnuCOBOL compiler is free software: you can redistribute it
## and/or modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## GnuCOBOL is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with GnuCOBOL.  If not, see <https://www.gnu.org/licenses/>.

### GnuCOBOL Test Suite

## All references to $COBC changed to $COMPILE
##    Marty 1/9/23
## Note: cobc -c file / cobc --config=file
##  and  cobcrun -c file / cobcrun --config=file
##  are tested via configuration.at


AT_SETUP([compiler help and information])
AT_KEYWORDS([used-binaries runmisc cobc])
# SOME KEYWORDS are NOT IMPLEMENTED. Commented out. TODO

# FIXME: check at least some parts of the output by using $GREP
AT_CHECK([$COMPILE --version], [0], [ignore], [])
AT_CHECK([$COMPILE -v --version], [0], [ignore], [ignore])
AT_CHECK([$COMPILE --help], [0], [ignore], [])
AT_CLEANUP


AT_SETUP([compiler warnings])
AT_KEYWORDS([used-binaries runmisc cobc warning])
AT_SKIP_IF(true)
# Gnu Unique -- warnings checking will be important TODO

AT_DATA([prog.cob],[
        IDENTIFICATION DIVISION.
        PROGRAM-ID. prog.
        DATA DIVISION.
        WORKING-STORAGE SECTION.
        77 var1    pic 9.
        77 var2    pic 99.
        PROCEDURE DIVISION.
        PERFORM UNTIL 0 = 1
           DISPLAY 'BLA'
        END-PERFORM.
        MOVE var1 to var2
        MOVE var2 to var1
        GOBACK.
])
AT_CHECK([$COMPILE_ONLY prog.cob], [0], [], [])

AT_CLEANUP


AT_SETUP([compiler outputs (general)])
AT_KEYWORDS([used-binaries runmisc cobc])
# Leaving in otherwise OK flag tests

AT_DATA([prog.cob],[
        IDENTIFICATION DIVISION.
        PROGRAM-ID. prog.
        DATA DIVISION.
        WORKING-STORAGE SECTION.
        01 BLA PIC X(5) VALUE 'bluBb'.
        PROCEDURE DIVISION.
        DISPLAY BLA NO ADVANCING END-DISPLAY
        STOP RUN.
])

AT_CHECK([$COMPILE -C prog.cob], [0], [ignore], [ignore])
AT_CHECK([$COMPILE -v -c prog.c], [0], [ignore], [ignore])
AT_CLEANUP


AT_SETUP([compiler outputs (file specified)])
AT_KEYWORDS([used-binaries runmisc cobc gen-c-line-directives gen-c-labels gen line labels])
AT_SKIP_IF(true)
# Gnu Unique -- flag checking will be important TODO

AT_DATA([prog.cob],[
        IDENTIFICATION DIVISION.
        PROGRAM-ID. prog.
        DATA DIVISION.
        WORKING-STORAGE SECTION.
        01 BLA PIC X(5) VALUE 'bluBb'.
        PROCEDURE DIVISION.
        MAIN-PROC SECTION.
        COPY PROC.
        END-PROC  SECTION.
        STOP RUN.
])

AT_CHECK([mkdir -p sub/copy], [0], [], [])

AT_DATA([sub/copy/PROC.cpy],[
        DISPLAY BLA NO ADVANCING.
])

AT_CHECK([$COMPILE -I sub/copy prog.cob -o prog], [0], [], [])
AT_CHECK([./prog], [0], [bluBb], [])
AT_CLEANUP


AT_SETUP([compiler outputs (path specified)])
AT_KEYWORDS([used-binaries runmisc cobc])
# Commented out Gnu-isms

AT_DATA([prog.cob],[
        IDENTIFICATION DIVISION.
        PROGRAM-ID. prog.
        DATA DIVISION.
        WORKING-STORAGE SECTION.
        01 BLA PIC X(5) VALUE 'bluBb'.
        PROCEDURE DIVISION.
        DISPLAY BLA NO ADVANCING END-DISPLAY
        STOP RUN.
])

AT_CHECK([mkdir -p sub], [0], [], [])
AT_CHECK([$COMPILE prog.cob -o sub/prog], [0], [], [])
AT_CHECK([./sub/prog], [0], [bluBb], [])
AT_CLEANUP


AT_SETUP([compiler outputs (assembler)])
AT_KEYWORDS([used-binaries runmisc cobc])
AT_SKIP_IF(true)
# Not producing assembler out TODO

AT_DATA([prog.cob],[
        IDENTIFICATION DIVISION.
        PROGRAM-ID. prog.
        DATA DIVISION.
        WORKING-STORAGE SECTION.
        01 BLA PIC X(5) VALUE 'bluBb'.
        PROCEDURE DIVISION.
        DISPLAY BLA NO ADVANCING END-DISPLAY
        STOP RUN.
])

AT_CHECK([$COMPILE -v -S prog.cob], [0], [ignore], [ignore])
AT_CHECK([test -f prog.s], [0], [], [],
# Previous test "failed" --> prog.s not available --> likely a VS build
# only check for file as cl.exe cannot create executables from self-created
# assembler sources
[AT_CHECK([test -f prog.asm], [0], [], [])],
# Previous test "passed" --> prog.s is available, test compilation and run
[AT_CHECK([$COMPILE -v prog.s], [0], [ignore], [ignore])
 AT_CHECK([$COBCRUN prog], [0], [bluBb], [])])
AT_CHECK([$COMPILE -v -x -S prog.cob], [0], [ignore], [ignore])
AT_CHECK([test -f prog.s], [0], [], [],
# Previous test "failed" --> prog.s not available --> likely a VS build
# only check for file as cl.exe cannot create executables from self-created
# assembler sources
[AT_CHECK([test -f prog.asm], [0], [], [])],
# Previous test "passed" --> prog.s is available, test compilation and run
[AT_CHECK([$COMPILE -v -x prog.s], [0], [ignore], [ignore])
 AT_CHECK([./prog], [0], [bluBb], [])])
AT_CLEANUP


AT_SETUP([source file not found])
AT_KEYWORDS([used-binaries cobc runmisc])

# Output corrected: MH 1/9/23
AT_CHECK([$COMPILE_ONLY prog.cob], [1], [],
[cobol1: cannot open filename prog.cob: No such file or directory: No such file or directory
])

AT_CLEANUP


AT_SETUP([temporary path invalid])
AT_KEYWORDS([used-binaries cobc runmisc])
AT_SKIP_IF(true)
# Gnu Unique -- flag checking will be important TODO

# Note: may be either removed completely as there was a report about
#       this test "failing" - or skipped as this very often fails for
#       WIN32 builds

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       PROCEDURE        DIVISION.
           DISPLAY "OK" NO ADVANCING
           END-DISPLAY.
           EXIT PROGRAM.
])

AT_CHECK([TMPDIR="" TMP="notthere" TEMP="" $COMPILE prog.cob], [0], [],
[libcob: warning: Temporary directory TMP is invalid, adjust TMPDIR!
])
AT_CHECK([./prog], [0], [OK], [])
AT_CHECK([TMPDIR="" TMP="" TEMP="./prog.cob" $COMPILE prog.cob], [0], [],
[libcob: warning: Temporary directory TEMP is invalid, adjust TMPDIR!
])

AT_CLEANUP


AT_SETUP([use of full path for cobc])
AT_KEYWORDS([used-binaries runmisc])
AT_SKIP_IF(true)
# Gnu Unique

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       PROCEDURE        DIVISION.
           DISPLAY "OK" NO ADVANCING
           END-DISPLAY.
           EXIT PROGRAM.
])

AT_CHECK([$COMPILE "$(_return_path "$(pwd)/prog.cob")"], [0], [], [])
AT_CHECK([./a.out], [0], [OK], [])

AT_CLEANUP


AT_SETUP([C Compiler optimizations])
AT_KEYWORDS([used-binaries runmisc compiler optimization])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       PROCEDURE        DIVISION.
           DISPLAY "OK" NO ADVANCING
           END-DISPLAY.
           EXIT PROGRAM.
])

# removed -v from $COMPILE steps
AT_CHECK([$COMPILE -O  -o prog  prog.cob], [0], [ignore], [ignore])
AT_CHECK([./prog],  [0], [OK], [])

AT_CHECK([$COMPILE -O2 -o prog2 prog.cob], [0], [ignore], [ignore])
AT_CHECK([./prog2], [0], [OK], [])

AT_CHECK([$COMPILE -Os -o progs prog.cob], [0], [ignore], [ignore])
AT_CHECK([./progs], [0], [OK], [])

AT_CHECK([$COMPILE -O3 -o prog3 prog.cob], [0], [ignore], [ignore])
AT_CHECK([./prog3], [0], [OK], [])

AT_CHECK([$COMPILE -O0  -o prog  prog.cob], [0], [ignore], [ignore])
AT_CHECK([./prog],  [0], [OK], [])

AT_CLEANUP


AT_SETUP([invalid compiler option])
AT_KEYWORDS([used-binaries runmisc compiler option])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
])

# normal option
AT_CHECK([$COMPILE --thisoptiondoesntexist prog.cob], [1], [],
[gcobol: error: unrecognized command-line option '--thisoptiondoesntexist'
])
# flag
AT_CHECK([$COMPILE -flagdoesntexist prog.cob], [1], [],
[gcobol: error: unrecognized command-line option '-flagdoesntexist'
])

AT_CLEANUP


AT_SETUP([cobcrun help and information])
AT_KEYWORDS([used-binaries runmisc cobcrun])
AT_SKIP_IF(true)
# Gnu UNIQUE

# FIXME: check at least some parts of the output by using $GREP
AT_CHECK([$COBCRUN --version], [0], [ignore], [])
AT_CHECK([$COBCRUN -v --version], [0], [ignore], [])
AT_CHECK([$COBCRUN -q --version], [0], [ignore], [])
AT_CHECK([$COBCRUN --help], [0], [ignore], [])
AT_CHECK([$COBCRUN --info], [0], [ignore], [])
AT_CHECK([$COBCRUN -q --info], [0], [ignore], [])
AT_CLEANUP


AT_SETUP([cobcrun validation])
AT_KEYWORDS([used-binaries runmisc])
AT_SKIP_IF(true)
# Gnu UNIQUE

AT_DATA([callee.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      callee.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 WRK-VAR       PIC X(5).
       01 EXT-VAR       PIC X(5) EXTERNAL.
       PROCEDURE        DIVISION.
           DISPLAY EXT-VAR NO ADVANCING
           END-DISPLAY.
           MOVE "World" TO EXT-VAR.
           EXIT PROGRAM.
])

AT_DATA([caller.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      caller.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 EXT-VAR       PIC X(5) EXTERNAL.
       01 WRK-VAR       PIC X(5).
       PROCEDURE        DIVISION.
           MOVE "Hello" TO EXT-VAR.
           CALL "callee"
           END-CALL.
           DISPLAY EXT-VAR NO ADVANCING
           END-DISPLAY.
           STOP RUN.
])

AT_CHECK([$COMPILE_MODULE callee.cob], [0], [], [])
AT_CHECK([$COMPILE_MODULE caller.cob], [0], [], [])
AT_CHECK([$COBCRUN caller], [0], [HelloWorld], [])

AT_CLEANUP


AT_SETUP([cobcrun -M DSO entry argument])
AT_KEYWORDS([used-binaries runmisc])
AT_SKIP_IF(true)
# Gnu UNIQUE - dynamic loading of .so

AT_DATA([callee.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      callee.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 EXT-VAR       PIC X(5) EXTERNAL.
       PROCEDURE        DIVISION.
           DISPLAY EXT-VAR END-DISPLAY.
           MOVE "World" TO EXT-VAR.
           EXIT PROGRAM.
])

AT_DATA([caller.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      caller.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 EXT-VAR       PIC X(5) EXTERNAL.
       PROCEDURE        DIVISION.
           MOVE "Hello" TO EXT-VAR.
           CALL "callee" END-CALL.
           DISPLAY EXT-VAR END-DISPLAY.
           STOP RUN.
       END PROGRAM caller.

       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      inside.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 EXT-VAR       PIC X(5) EXTERNAL.
       01 CLA-VAR       PIC X(5).
       PROCEDURE        DIVISION.
           MOVE "Aloha" TO EXT-VAR.
           CALL "callee" END-CALL.
           DISPLAY EXT-VAR END-DISPLAY.
           ACCEPT CLA-VAR FROM COMMAND-LINE END-ACCEPT.
           DISPLAY CLA-VAR END-DISPLAY.
           STOP RUN.
       END PROGRAM inside.
])

AT_CHECK([$COMPILE_MODULE callee.cob], [0], [], [])
AT_CHECK([$COMPILE_MODULE caller.cob], [0], [], [])
AT_CHECK([$COBCRUN -M ./caller inside again], [0],
[Aloha
World
again
], [])

AT_CLEANUP


## FIXME: missing tests:
## * combining $COBCRUN -M with COB_PRE_LOAD / COB_LIBRARY_PATH
## * $COBCRUN -m


AT_SETUP([cobcrun -M directory/ default])
AT_KEYWORDS([used-binaries runmisc])
AT_SKIP_IF(true)
# GNU unique - cobcrun

AT_DATA([callee.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      callee.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 EXT-VAR       PIC X(5) EXTERNAL.
       PROCEDURE        DIVISION.
           DISPLAY EXT-VAR END-DISPLAY.
           MOVE "World" TO EXT-VAR.
           EXIT PROGRAM.
])

AT_DATA([caller.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      caller.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 EXT-VAR       PIC X(5) EXTERNAL.
       PROCEDURE        DIVISION.
           MOVE "Hello" TO EXT-VAR.
           CALL "callee" END-CALL.
           DISPLAY EXT-VAR END-DISPLAY.
           STOP RUN.
])

AT_CHECK([$COMPILE_MODULE callee.cob], [0], [], [])
AT_CHECK([$COMPILE_MODULE caller.cob], [0], [], [])
AT_CHECK([$COBCRUN -M ./ caller], [0],
[Hello
World
], [])


AT_CLEANUP


AT_SETUP([cobcrun -M directory/dso alternate])
AT_KEYWORDS([used-binaries runmisc])
AT_SKIP_IF(true)
# GNU unique - cobcrun

AT_DATA([callee.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      callee.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 EXT-VAR       PIC X(5) EXTERNAL.
       PROCEDURE        DIVISION.
           DISPLAY EXT-VAR END-DISPLAY.
           MOVE "World" TO EXT-VAR.
           EXIT PROGRAM.
])

AT_DATA([caller.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      caller.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 EXT-VAR       PIC X(5) EXTERNAL.
       PROCEDURE        DIVISION.
           MOVE "Hello" TO EXT-VAR.
           CALL "callee" END-CALL.
           DISPLAY EXT-VAR END-DISPLAY.
           STOP RUN.
       END PROGRAM caller.

       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      inside.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 EXT-VAR       PIC X(5) EXTERNAL.
       PROCEDURE        DIVISION.
           MOVE "Aloha" TO EXT-VAR.
           CALL "callee" END-CALL.
           DISPLAY EXT-VAR END-DISPLAY.
           STOP RUN.
       END PROGRAM inside.
])

AT_CHECK([$COMPILE_MODULE callee.cob], [0], [], [])
AT_CHECK([$COMPILE_MODULE caller.cob], [0], [], [])
AT_CHECK([$COBCRUN -M ./caller inside], [0],
[Aloha
World
], [])

AT_CLEANUP


AT_SETUP([cobcrun -M DSO entry multiple arguments])
AT_KEYWORDS([used-binaries runmisc])
AT_SKIP_IF(true)
# GNU unique - cobcrun

# Test that modules can be called with ARGUMENT-VALUES
AT_DATA([called.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      called.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 CLI-ARGS      PIC X(27).
       01 ARG-TWO       PIC X(6).
       PROCEDURE        DIVISION.
           ACCEPT CLI-ARGS FROM COMMAND-LINE END-ACCEPT.
           DISPLAY 2 UPON ARGUMENT-NUMBER END-DISPLAY.
           ACCEPT ARG-TWO FROM ARGUMENT-VALUE END-ACCEPT.
           DISPLAY CLI-ARGS ":" ARG-TWO END-DISPLAY.
           EXIT PROGRAM.
])

AT_DATA([mainer.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      mainer.
       PROCEDURE        DIVISION.
           STOP RUN.
       END PROGRAM mainer.
])

AT_CHECK([$COMPILE -b ${FLAGS} mainer.cob called.cob], [0], [], [])
AT_CHECK([$COBCRUN -M ./mainer called "first argument" "second" "third"], [0],
[first argument second third:second
], [])

# additional test with environment configuration settings removed:
AT_CHECK([unset COB_PRE_LOAD COB_LIBRARY_PATH ; \
   $COBCRUN -M ./mainer called "first argument" "second" "third"], [0],
[first argument second third:second
], [])

# additional test with showing the preloaded environment
# FIXME: check at least some parts of the output ("configuration" and the expected output) by using $GREP
AT_CHECK([$COBCRUN -M ./mainer --runtime-conf called "first argument" "second" "third"], [0],
[ignore], [])

AT_CLEANUP


AT_SETUP([cobcrun error messages])
AT_KEYWORDS([used-binaries runmisc])
AT_SKIP_IF(true)
# GNU unique - cobcrun

AT_CHECK([$COBCRUN -q], [1], [],
[cobcrun: missing PROGRAM name
Try 'cobcrun --help' for more information.
])
AT_CHECK([$COBCRUN -q -prog], [1], [],
[cobcrun: unrecognized option '-prog'
])
AT_CHECK([$COBCRUN noprog], [1], [],
[libcob: error: module 'noprog' not found
])
AT_CHECK([$COBCRUN -q -M], [1], [],
[cobcrun: option requires an argument -- 'M'
])

AT_CLEANUP


AT_SETUP([run job after compilation])
AT_KEYWORDS([used-binaries runmisc cobcrun])
AT_SKIP_IF(true)
# GNU unique - cobcrun

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       PROCEDURE        DIVISION.
           DISPLAY "job" WITH NO ADVANCING END-DISPLAY
           STOP RUN WITH NORMAL STATUS.
])

AT_CHECK([$COMPILE -jd prog.cob], [0], [job], [])
AT_CHECK([$COMPILE_MODULE -jd prog.cob], [0], [job], [])

AT_CLEANUP


AT_SETUP([run job after compilation (path specified)])
AT_KEYWORDS([used-binaries runmisc cobcrun])
AT_SKIP_IF(true)
# GNU unique - cobcrun

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       PROCEDURE        DIVISION.
           DISPLAY "job" WITH NO ADVANCING END-DISPLAY
           STOP RUN WITH NORMAL STATUS.
])

AT_CHECK([mkdir -p sub], [0], [], [])
AT_CHECK([$COMPILE_MODULE -jd -o $(_return_path "sub/prog") prog.cob], [0], [job], [])
AT_CHECK([$COMPILE -jd -o $(_return_path "sub/prog$COB_EXE_EXT") prog.cob], [0], [job], [])

AT_CLEANUP


AT_SETUP([run job with optional arguments])
AT_KEYWORDS([used-binaries runmisc cobcrun])
AT_SKIP_IF(true)
# GNU unique - cobcrun

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 CLI           PIC X(8).
       PROCEDURE        DIVISION.
           ACCEPT CLI FROM COMMAND-LINE
           DISPLAY CLI WITH NO ADVANCING END-DISPLAY
           STOP RUN WITH NORMAL STATUS.
])

AT_CHECK([$COMPILE -j="job 123" prog.cob], [0], [job 123 ], [])
AT_CHECK([$COMPILE -jdg prog.cob], [0], [        ], [])
AT_CHECK([$COMPILE_MODULE --job=job123 prog.cob], [0], [job123  ], [])

AT_CLEANUP


AT_SETUP([compile from stdin])
AT_KEYWORDS([used-binaries runmisc cobc cobcrun])
AT_SKIP_IF(true)
# GNU unique - cobcrun

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      a.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       PROCEDURE        DIVISION.
           STOP RUN WITH NORMAL STATUS.
])

AT_CHECK([cat prog.cob | $COMPILE_MODULE -], [0], [], [],
# Previous test failed run again with full verbose output
# leading to an (expected) fail, but with decent messages
[AT_CHECK([cat prog.cob | $COMPILE_MODULE -vv -], [0], [])]
)
AT_CHECK([$COBCRUN a], [0], [], [])

AT_CHECK([cat prog.cob | $COMPILE -], [0], [], [],
# more to debug ...
[AT_CHECK([cat prog.cob | $COMPILE -vv -], [0], [])]
)
AT_CHECK([./a.out], [0], [], [])



AT_CLEANUP


AT_SETUP([run job after compilation from stdin])
AT_KEYWORDS([used-binaries runmisc cobc cobcrun])
AT_SKIP_IF(true)
# GNU unique - cobcrun

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      a.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       PROCEDURE        DIVISION.
           DISPLAY "job" WITH NO ADVANCING END-DISPLAY
           STOP RUN WITH NORMAL STATUS.
])

AT_CHECK([cat prog.cob | $COMPILE -j -], [0], [job], [],
# Previous test failed run again with full verbose output
# leading to an (expected) fail, but with decent messages
[AT_CHECK([cat prog.cob | $COMPILE -vv -j -], [0], [])]
)
AT_CHECK([cat prog.cob | $COMPILE_MODULE -j -], [0], [job], [],
# more to debug ...
[AT_CHECK([cat prog.cob | $COMPILE_MODULE -vv -j -], [0], [])]
)

AT_CLEANUP
