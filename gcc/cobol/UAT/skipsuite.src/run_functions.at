## Copyright (C) 2003-2012, 2014-2020 Free Software Foundation, Inc.
## Written by Keisuke Nishida, Roger While, Simon Sobisch, Edward Hart
##
## This file is part of GnuCOBOL.
##
## The GnuCOBOL compiler is free software: you can redistribute it
## and/or modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## GnuCOBOL is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with GnuCOBOL.  If not, see <https://www.gnu.org/licenses/>.

### GnuCOBOL Test Suite

### ISO+IEC+1989-2002 15 Intrinsic Functions
### ISO+IEC+1989-2002 9.4 User-Defined Functions

AT_SETUP([FUNCTION CONTENT-LENGTH])
AT_KEYWORDS([functions length])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01  P            USAGE    POINTER.
       01  X            PIC      X(4) VALUE Z"ABC".
       01  TEST-FLD     USAGE    BINARY-LONG.
       PROCEDURE        DIVISION.
           MOVE FUNCTION CONTENT-LENGTH ( P )
             TO TEST-FLD.
           IF TEST-FLD NOT = 0
              DISPLAY 'CONTENT-LENGTH NULL wrong: ' TEST-FLD
              END-DISPLAY
           END-IF
           SET P TO ADDRESS OF X
           MOVE FUNCTION CONTENT-LENGTH ( P )
             TO TEST-FLD
           IF TEST-FLD NOT = 3
              DISPLAY 'CONTENT-LENGTH z"abc" wrong: ' TEST-FLD
              END-DISPLAY
           END-IF
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0], [], [])

AT_CLEANUP


AT_SETUP([FUNCTION CONTENT-OF])
AT_KEYWORDS([functions POINTER literal BASED ALLOCATE FREE EXCEPTION-STATUS])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01  P   USAGE    POINTER.
       01  X   PIC      X(4) VALUE Z"ABC".
       01  B   PIC      X(10) BASED.
       PROCEDURE        DIVISION.
           SET P TO ADDRESS OF X
           IF FUNCTION CONTENT-OF ( P ) NOT EQUAL 'ABC' THEN
              DISPLAY 'CONTENT-OF(ptr) wrong' END-DISPLAY
           END-IF
           IF FUNCTION CONTENT-OF ( P, 2 ) NOT EQUAL 'AB' THEN
              DISPLAY 'CONTENT-OF(ptr, len) wrong' END-DISPLAY
           END-IF
           IF FUNCTION EXCEPTION-STATUS NOT = SPACES THEN
              DISPLAY 'unexpected exception (1): '
                       FUNCTION EXCEPTION-STATUS
              END-DISPLAY
           END-IF
           SET  P      TO NULL
           MOVE 'PPPP' TO X
           STRING FUNCTION CONTENT-OF ( P )
                  DELIMITED BY SIZE
                  INTO X
           END-STRING
      *>   Note: result *should* depend on dialect option zero-length literals
           IF X NOT EQUAL 'PPPP' THEN
              DISPLAY 'CONTENT-OF empty POINTER wrong: "'" X "'"
              END-DISPLAY
           END-IF
           IF FUNCTION EXCEPTION-STATUS NOT = "EC-DATA-PTR-NULL" THEN
              DISPLAY 'missing exception (1)'
              END-DISPLAY
           END-IF
           ALLOCATE B INITIALIZED
           SET  P      TO ADDRESS OF B
           IF FUNCTION CONTENT-OF ( P, 1 ) NOT EQUAL SPACES THEN
              DISPLAY 'CONTENT-OF allocated BASED item wrong'
              END-DISPLAY
           END-IF
           IF FUNCTION EXCEPTION-STATUS NOT = SPACES THEN
              DISPLAY 'unexpected exception (2): '
                       FUNCTION EXCEPTION-STATUS
              END-DISPLAY
           END-IF
           FREE B
           SET  P      TO ADDRESS OF B
           MOVE 'BBBB' TO X
           STRING FUNCTION CONTENT-OF ( P )
                  DELIMITED BY SIZE
                  INTO X
           END-STRING
      *>   Note: result *should* depend on dialect option zero-length literals
           IF X NOT EQUAL 'BBBB' THEN
              DISPLAY 'CONTENT-OF unallocated BASED item wrong: "' X '"'
              END-DISPLAY
           END-IF
           IF FUNCTION EXCEPTION-STATUS NOT = "EC-DATA-PTR-NULL" THEN
              DISPLAY 'missing exception (2)'
              END-DISPLAY
           END-IF
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0], [], [])
AT_CLEANUP


AT_SETUP([FUNCTION CURRENCY-SYMBOL])
AT_KEYWORDS([functions])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       ENVIRONMENT      DIVISION.
       DATA             DIVISION.
       WORKING-STORAGE SECTION.
       01  TEST-FLD     PIC X(8) VALUE SPACES.
       PROCEDURE        DIVISION.
           MOVE FUNCTION CURRENCY-SYMBOL TO TEST-FLD.
           DISPLAY "OK" NO ADVANCING
           END-DISPLAY
           STOP RUN.
])
AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0], [OK], [])
AT_CLEANUP


AT_SETUP([FUNCTION MODULE-CALLER-ID])
AT_KEYWORDS([functions])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       ENVIRONMENT      DIVISION.
       DATA             DIVISION.
       WORKING-STORAGE SECTION.
       PROCEDURE        DIVISION.
           CALL "prog2"
           END-CALL.
           STOP RUN.
])

AT_DATA([prog2.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog2.
       ENVIRONMENT      DIVISION.
       DATA             DIVISION.
       WORKING-STORAGE SECTION.
       PROCEDURE        DIVISION.
           DISPLAY FUNCTION MODULE-CALLER-ID NO ADVANCING
           END-DISPLAY.
           EXIT PROGRAM.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COMPILE_MODULE prog2.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0], [prog])

AT_CLEANUP


AT_SETUP([FUNCTION MODULE-DATE])
AT_KEYWORDS([functions])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       ENVIRONMENT      DIVISION.
       DATA             DIVISION.
       WORKING-STORAGE SECTION.
       01  TEST-DATE    PIC 9(8) VALUE 0.
       PROCEDURE        DIVISION.
           MOVE FUNCTION MODULE-DATE TO TEST-DATE.
           IF   TEST-DATE NOT = 0
             DISPLAY "OK" NO ADVANCING
             END-DISPLAY
           END-IF.
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0], [OK], [])

AT_CLEANUP


AT_SETUP([FUNCTION MODULE-FORMATTED-DATE])
AT_KEYWORDS([functions])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       ENVIRONMENT      DIVISION.
       DATA             DIVISION.
       WORKING-STORAGE SECTION.
       01  TEST-DATE    PIC X(16) VALUE SPACES.
       PROCEDURE        DIVISION.
           MOVE FUNCTION MODULE-FORMATTED-DATE TO TEST-DATE.
           IF   TEST-DATE NOT = SPACES
             DISPLAY "OK" NO ADVANCING
             END-DISPLAY
           END-IF.
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0], [OK], [])

AT_CLEANUP


AT_SETUP([FUNCTION MODULE-ID])
AT_KEYWORDS([functions])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       ENVIRONMENT      DIVISION.
       DATA             DIVISION.
       WORKING-STORAGE SECTION.
       PROCEDURE        DIVISION.
           DISPLAY FUNCTION MODULE-ID NO ADVANCING
           END-DISPLAY.
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0], [prog])

AT_CLEANUP


AT_SETUP([FUNCTION MODULE-PATH])
AT_KEYWORDS([functions])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       ENVIRONMENT      DIVISION.
       DATA             DIVISION.
       WORKING-STORAGE SECTION.
       01  TEST-PATH    PIC X(16) VALUE SPACES.
       PROCEDURE        DIVISION.
           MOVE FUNCTION MODULE-PATH TO TEST-PATH.
           IF   TEST-PATH NOT = SPACES
             DISPLAY "OK" NO ADVANCING
             END-DISPLAY
           END-IF.
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0], [OK], [])

AT_CLEANUP


AT_SETUP([FUNCTION MODULE-SOURCE])
AT_KEYWORDS([functions])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       ENVIRONMENT      DIVISION.
       DATA             DIVISION.
       WORKING-STORAGE SECTION.
       PROCEDURE        DIVISION.
           DISPLAY FUNCTION MODULE-SOURCE NO ADVANCING
           END-DISPLAY.
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0], [prog.cob])

AT_CLEANUP


AT_SETUP([FUNCTION MODULE-TIME])
AT_KEYWORDS([functions])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       ENVIRONMENT      DIVISION.
       DATA             DIVISION.
       WORKING-STORAGE SECTION.
       01  TEST-TIME    PIC 9(6) VALUE 0.
       PROCEDURE        DIVISION.
           MOVE FUNCTION MODULE-TIME TO TEST-TIME.
           IF   TEST-TIME NOT = 0
             DISPLAY "OK" NO ADVANCING
             END-DISPLAY
           END-IF.
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0], [OK], [])

AT_CLEANUP


AT_SETUP([FUNCTION MONETARY-DECIMAL-POINT])
AT_KEYWORDS([functions])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       ENVIRONMENT      DIVISION.
       DATA             DIVISION.
       WORKING-STORAGE SECTION.
       01  TEST-FLD     PIC X(8) VALUE SPACES.
       PROCEDURE        DIVISION.
           MOVE FUNCTION MONETARY-DECIMAL-POINT TO TEST-FLD.
           DISPLAY "OK" NO ADVANCING
           END-DISPLAY
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0], [OK], [])

AT_CLEANUP


AT_SETUP([FUNCTION MONETARY-THOUSANDS-SEPARATOR])
AT_KEYWORDS([functions])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       ENVIRONMENT      DIVISION.
       DATA             DIVISION.
       WORKING-STORAGE SECTION.
       01  TEST-FLD     PIC X(8) VALUE SPACES.
       PROCEDURE        DIVISION.
           MOVE FUNCTION MONETARY-THOUSANDS-SEPARATOR TO TEST-FLD.
           DISPLAY "OK" NO ADVANCING
           END-DISPLAY
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0], [OK], [])

AT_CLEANUP


AT_SETUP([FUNCTION NUMERIC-DECIMAL-POINT])
AT_KEYWORDS([functions])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       ENVIRONMENT      DIVISION.
       DATA             DIVISION.
       WORKING-STORAGE SECTION.
       01  TEST-FLD     PIC X(8) VALUE SPACES.
       PROCEDURE        DIVISION.
           MOVE FUNCTION NUMERIC-DECIMAL-POINT TO TEST-FLD.
           DISPLAY "OK" NO ADVANCING
           END-DISPLAY
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0], [OK], [])

AT_CLEANUP


AT_SETUP([FUNCTION NUMERIC-THOUSANDS-SEPARATOR])
AT_KEYWORDS([functions])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       ENVIRONMENT      DIVISION.
       DATA             DIVISION.
       WORKING-STORAGE SECTION.
       01  TEST-FLD     PIC X(8) VALUE SPACES.
       PROCEDURE        DIVISION.
           MOVE FUNCTION NUMERIC-THOUSANDS-SEPARATOR TO TEST-FLD.
           DISPLAY "OK" NO ADVANCING
           END-DISPLAY
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0], [OK], [])

AT_CLEANUP


AT_SETUP([FUNCTION STORED-CHAR-LENGTH])
AT_KEYWORDS([functions])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01  Y   PIC   X(24).
       01  Z   USAGE BINARY-LONG.
       PROCEDURE        DIVISION.
           MOVE "123456789012" TO Y.
           MOVE FUNCTION STORED-CHAR-LENGTH ( Y ) TO Z.
           IF Z NOT = 12
              DISPLAY Z
              END-DISPLAY
           END-IF.
           STOP RUN.
])
AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0], [], [])
AT_CLEANUP




AT_SETUP([FORMATTED-(DATE)TIME with SYSTEM-OFFSET])
AT_KEYWORDS([functions FORMATTED-TIME FORMATTED-DATETIME extensions])
AT_XFAIL_IF([test "$COB_DIALECT" != "gnu"])

AT_DATA([prog.cob], [
       IDENTIFICATION  DIVISION.
       PROGRAM-ID.     prog.
       DATA            DIVISION.
       WORKING-STORAGE SECTION.
       01  str         PIC X(30).
       77  val         pic 9(02).

       PROCEDURE DIVISION.
           MOVE FUNCTION FORMATTED-DATETIME
                    ("YYYYDDDThhmmss+hhmm", 1, 45296, SYSTEM-OFFSET)
               TO str
           MOVE FUNCTION TEST-FORMATTED-DATETIME
                   ("YYYYDDDThhmmss+hhmm", str) TO val
           IF val not = 0
               DISPLAY "Test 1 failed: " str ' - ' val END-DISPLAY
           END-IF

           MOVE FUNCTION FORMATTED-TIME
                    ("hhmmss.ssZ", 45296, SYSTEM-OFFSET)
               TO str
           MOVE FUNCTION TEST-FORMATTED-DATETIME
                   ("hhmmss.ssZ", str) TO val
           IF val not = 0
               DISPLAY "Test 2 failed: " str ' - ' val END-DISPLAY
           END-IF
           .
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([$COBCRUN_DIRECT ./a.out], [0])

AT_CLEANUP


