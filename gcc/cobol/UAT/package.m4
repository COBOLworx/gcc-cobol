# Package.m4 for COBOL for GCC

# Signature of the current package.
m4_define([AT_PACKAGE_NAME],      [COBOL for GCC])
# TODO - definitive TARNAME and Version (replacing preview)
m4_define([AT_PACKAGE_TARNAME],   [gcobol.tar])
m4_define([AT_PACKAGE_VERSION],   [preview])
m4_define([AT_PACKAGE_STRING],    [COBOL for GCC - preview])
m4_define([AT_PACKAGE_BUGREPORT], [bugs@cobolworx.com])
m4_define([AT_PACKAGE_URL],       [https://www.cobolworx.com])

