## Copyright (C) 2003-2012, 2014-2019 Free Software Foundation, Inc.
## Copyright (C) 2020-2023 COBOLworx.
## Written by Keisuke Nishida, Roger While, Simon Sobisch, Ron Norman
##
## Copyright (C) 2003-2012, 2015-2018, 2020 Free Software Foundation, Inc.
## Written by Keisuke Nishida, Roger While, Simon Sobisch
##
## This file is part of GnuCOBOL.
##
## The GnuCOBOL compiler is free software: you can redistribute it
## and/or modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## GnuCOBOL is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with GnuCOBOL.  If not, see <https://www.gnu.org/licenses/>.

## This file was copied from GnuCOBOL and modified for use with COBOL
## for GCC (gcobol).
## Modifications were made by Marty Heyman and Nick Mower
##

### COBOL for GCC Test Suite

### PACKED-DECIMAL


# dump
AT_SETUP([PACKED-DECIMAL dump])
AT_KEYWORDS([packed])
AT_SKIP_IF(false)
# Gnu dynamic load not supported
# Gnu TEMPLATE -- replacement needed TODO
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 G-1.
         02 X-1         PIC 9(1) VALUE 1
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-2.
         02 X-2         PIC 9(2) VALUE 12
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-3.
         02 X-3         PIC 9(3) VALUE 123
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-4.
         02 X-4         PIC 9(4) VALUE 1234
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-5.
         02 X-5         PIC 9(5) VALUE 12345
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-6.
         02 X-6	        PIC 9(6) VALUE 123456
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-7.
         02 X-7         PIC 9(7) VALUE 1234567
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-8.
         02 X-8         PIC 9(8) VALUE 12345678
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-9.
         02 X-9         PIC 9(9) VALUE 123456789
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-10.
         02 X-10        PIC 9(10) VALUE 1234567890
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-11.
         02 X-11        PIC 9(11) VALUE 12345678901
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-12.
         02 X-12        PIC 9(12) VALUE 123456789012
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-13.
         02 X-13        PIC 9(13) VALUE 1234567890123
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-14.
         02 X-14        PIC 9(14) VALUE 12345678901234
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-15.
         02 X-15        PIC 9(15) VALUE 123456789012345
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-16.
         02 X-16        PIC 9(16) VALUE 1234567890123456
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-17.
         02 X-17        PIC 9(17) VALUE 12345678901234567
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-18.
         02 X-18        PIC 9(18) VALUE 123456789012345678
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S1.
         02 X-S1        PIC S9(1) VALUE -1
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S2.
         02 X-S2        PIC S9(2) VALUE -12
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S3.
         02 X-S3        PIC S9(3) VALUE -123
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S4.
         02 X-S4        PIC S9(4) VALUE -1234
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S5.
         02 X-S5        PIC S9(5) VALUE -12345
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S6.
         02 X-S6        PIC S9(6) VALUE -123456
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S7.
         02 X-S7        PIC S9(7) VALUE -1234567
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S8.
         02 X-S8        PIC S9(8) VALUE -12345678
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S9.
         02 X-S9        PIC S9(9) VALUE -123456789
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S10.
         02 X-S10       PIC S9(10) VALUE -1234567890
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S11.
         02 X-S11       PIC S9(11) VALUE -12345678901
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S12.
         02 X-S12       PIC S9(12) VALUE -123456789012
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S13.
         02 X-S13       PIC S9(13) VALUE -1234567890123
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S14.
         02 X-S14       PIC S9(14) VALUE -12345678901234
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S15.
         02 X-S15       PIC S9(15) VALUE -123456789012345
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S16.
         02 X-S16       PIC S9(16) VALUE -1234567890123456
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S17.
         02 X-S17       PIC S9(17) VALUE -12345678901234567
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       01 G-S18.
         02 X-S18       PIC S9(18) VALUE -123456789012345678
                        COMP-3.
         02 FILLER      PIC X(18) VALUE SPACE.
       PROCEDURE        DIVISION.
      *>   Dump all values
           CALL "dump" USING G-1
           END-CALL.
           CALL "dump" USING G-2
           END-CALL.
           CALL "dump" USING G-3
           END-CALL.
           CALL "dump" USING G-4
           END-CALL.
           CALL "dump" USING G-5
           END-CALL.
           CALL "dump" USING G-6
           END-CALL.
           CALL "dump" USING G-7
           END-CALL.
           CALL "dump" USING G-8
           END-CALL.
           CALL "dump" USING G-9
           END-CALL.
           CALL "dump" USING G-10
           END-CALL.
           CALL "dump" USING G-11
           END-CALL.
           CALL "dump" USING G-12
           END-CALL.
           CALL "dump" USING G-13
           END-CALL.
           CALL "dump" USING G-14
           END-CALL.
           CALL "dump" USING G-15
           END-CALL.
           CALL "dump" USING G-16
           END-CALL.
           CALL "dump" USING G-17
           END-CALL.
           CALL "dump" USING G-18
           END-CALL.
           CALL "dump" USING G-S1
           END-CALL.
           CALL "dump" USING G-S2
           END-CALL.
           CALL "dump" USING G-S3
           END-CALL.
           CALL "dump" USING G-S4
           END-CALL.
           CALL "dump" USING G-S5
           END-CALL.
           CALL "dump" USING G-S6
           END-CALL.
           CALL "dump" USING G-S7
           END-CALL.
           CALL "dump" USING G-S8
           END-CALL.
           CALL "dump" USING G-S9
           END-CALL.
           CALL "dump" USING G-S10
           END-CALL.
           CALL "dump" USING G-S11
           END-CALL.
           CALL "dump" USING G-S12
           END-CALL.
           CALL "dump" USING G-S13
           END-CALL.
           CALL "dump" USING G-S14
           END-CALL.
           CALL "dump" USING G-S15
           END-CALL.
           CALL "dump" USING G-S16
           END-CALL.
           CALL "dump" USING G-S17
           END-CALL.
           CALL "dump" USING G-S18
           END-CALL.
           INITIALIZE X-1.
           CALL "dump" USING G-1
           END-CALL.
           INITIALIZE X-2.
           CALL "dump" USING G-2
           END-CALL.
           INITIALIZE X-3.
           CALL "dump" USING G-3
           END-CALL.
           INITIALIZE X-4.
           CALL "dump" USING G-4
           END-CALL.
           INITIALIZE X-5.
           CALL "dump" USING G-5
           END-CALL.
           INITIALIZE X-6.
           CALL "dump" USING G-6
           END-CALL.
           INITIALIZE X-7.
           CALL "dump" USING G-7
           END-CALL.
           INITIALIZE X-8.
           CALL "dump" USING G-8
           END-CALL.
           INITIALIZE X-9.
           CALL "dump" USING G-9
           END-CALL.
           INITIALIZE X-10.
           CALL "dump" USING G-10
           END-CALL.
           INITIALIZE X-11.
           CALL "dump" USING G-11
           END-CALL.
           INITIALIZE X-12.
           CALL "dump" USING G-12
           END-CALL.
           INITIALIZE X-13.
           CALL "dump" USING G-13
           END-CALL.
           INITIALIZE X-14.
           CALL "dump" USING G-14
           END-CALL.
           INITIALIZE X-15.
           CALL "dump" USING G-15
           END-CALL.
           INITIALIZE X-16.
           CALL "dump" USING G-16
           END-CALL.
           INITIALIZE X-17.
           CALL "dump" USING G-17
           END-CALL.
           INITIALIZE X-18.
           CALL "dump" USING G-18
           END-CALL.
           INITIALIZE X-S1.
           CALL "dump" USING G-S1
           END-CALL.
           INITIALIZE X-S2.
           CALL "dump" USING G-S2
           END-CALL.
           INITIALIZE X-S3.
           CALL "dump" USING G-S3
           END-CALL.
           INITIALIZE X-S4.
           CALL "dump" USING G-S4
           END-CALL.
           INITIALIZE X-S5.
           CALL "dump" USING G-S5
           END-CALL.
           INITIALIZE X-S6.
           CALL "dump" USING G-S6
           END-CALL.
           INITIALIZE X-S7.
           CALL "dump" USING G-S7
           END-CALL.
           INITIALIZE X-S8.
           CALL "dump" USING G-S8
           END-CALL.
           INITIALIZE X-S9.
           CALL "dump" USING G-S9
           END-CALL.
           INITIALIZE X-S10.
           CALL "dump" USING G-S10
           END-CALL.
           INITIALIZE X-S11.
           CALL "dump" USING G-S11
           END-CALL.
           INITIALIZE X-S12.
           CALL "dump" USING G-S12
           END-CALL.
           INITIALIZE X-S13.
           CALL "dump" USING G-S13
           END-CALL.
           INITIALIZE X-S14.
           CALL "dump" USING G-S14
           END-CALL.
           INITIALIZE X-S15.
           CALL "dump" USING G-S15
           END-CALL.
           INITIALIZE X-S16.
           CALL "dump" USING G-S16
           END-CALL.
           INITIALIZE X-S17.
           CALL "dump" USING G-S17
           END-CALL.
           INITIALIZE X-S18.
           CALL "dump" USING G-S18
           END-CALL.
           MOVE ZERO TO X-1.
           CALL "dump" USING G-1
           END-CALL.
           MOVE ZERO TO X-2.
           CALL "dump" USING G-2
           END-CALL.
           MOVE ZERO TO X-3.
           CALL "dump" USING G-3
           END-CALL.
           MOVE ZERO TO X-4.
           CALL "dump" USING G-4
           END-CALL.
           MOVE ZERO TO X-5.
           CALL "dump" USING G-5
           END-CALL.
           MOVE ZERO TO X-6.
           CALL "dump" USING G-6
           END-CALL.
           MOVE ZERO TO X-7.
           CALL "dump" USING G-7
           END-CALL.
           MOVE ZERO TO X-8.
           CALL "dump" USING G-8
           END-CALL.
           MOVE ZERO TO X-9.
           CALL "dump" USING G-9
           END-CALL.
           MOVE ZERO TO X-10.
           CALL "dump" USING G-10
           END-CALL.
           MOVE ZERO TO X-11.
           CALL "dump" USING G-11
           END-CALL.
           MOVE ZERO TO X-12.
           CALL "dump" USING G-12
           END-CALL.
           MOVE ZERO TO X-13.
           CALL "dump" USING G-13
           END-CALL.
           MOVE ZERO TO X-14.
           CALL "dump" USING G-14
           END-CALL.
           MOVE ZERO TO X-15.
           CALL "dump" USING G-15
           END-CALL.
           MOVE ZERO TO X-16.
           CALL "dump" USING G-16
           END-CALL.
           MOVE ZERO TO X-17.
           CALL "dump" USING G-17
           END-CALL.
           MOVE ZERO TO X-18.
           CALL "dump" USING G-18
           END-CALL.
           MOVE ZERO TO X-S1.
           CALL "dump" USING G-S1
           END-CALL.
           MOVE ZERO TO X-S2.
           CALL "dump" USING G-S2
           END-CALL.
           MOVE ZERO TO X-S3.
           CALL "dump" USING G-S3
           END-CALL.
           MOVE ZERO TO X-S4.
           CALL "dump" USING G-S4
           END-CALL.
           MOVE ZERO TO X-S5.
           CALL "dump" USING G-S5
           END-CALL.
           MOVE ZERO TO X-S6.
           CALL "dump" USING G-S6
           END-CALL.
           MOVE ZERO TO X-S7.
           CALL "dump" USING G-S7
           END-CALL.
           MOVE ZERO TO X-S8.
           CALL "dump" USING G-S8
           END-CALL.
           MOVE ZERO TO X-S9.
           CALL "dump" USING G-S9
           END-CALL.
           MOVE ZERO TO X-S10.
           CALL "dump" USING G-S10
           END-CALL.
           MOVE ZERO TO X-S11.
           CALL "dump" USING G-S11
           END-CALL.
           MOVE ZERO TO X-S12.
           CALL "dump" USING G-S12
           END-CALL.
           MOVE ZERO TO X-S13.
           CALL "dump" USING G-S13
           END-CALL.
           MOVE ZERO TO X-S14.
           CALL "dump" USING G-S14
           END-CALL.
           MOVE ZERO TO X-S15.
           CALL "dump" USING G-S15
           END-CALL.
           MOVE ZERO TO X-S16.
           CALL "dump" USING G-S16
           END-CALL.
           MOVE ZERO TO X-S17.
           CALL "dump" USING G-S17
           END-CALL.
           MOVE ZERO TO X-S18.
           CALL "dump" USING G-S18
           END-CALL.
           STOP RUN.
           END PROGRAM prog.
        IDENTIFICATION   DIVISION.
        PROGRAM-ID.      dump.
        DATA             DIVISION.
        WORKING-STORAGE SECTION.
        01      HEXCHARS.
          02    HEXCHART PIC X(16) VALUE "0123456789abcdef".
          02    HEXCHAR  REDEFINES HEXCHART PIC X OCCURS 16.
        01      BYTE-TO-DUMP PIC X(1).
        01      FILLER.
          02    DUMPER1 PIC 9999 COMP-5.
          02    DUMPER2 REDEFINES DUMPER1 PIC X(1).
        01      THE-BYTE PIC 99.
        01      LADVANCE PIC 9.
        LINKAGE SECTION.
        01 G-VAL PIC X(20).
        01 G-PTR REDEFINES G-VAL USAGE POINTER.
        PROCEDURE DIVISION USING G-VAL.
        MOVE 1 TO THE-BYTE
        MOVE 0 TO LADVANCE
        PERFORM UNTIL THE-BYTE GREATER THAN 10
            MOVE G-VAL(THE-BYTE:1) TO BYTE-TO-DUMP
            IF THE-BYTE EQUAL TO 10 MOVE 1 TO LADVANCE END-IF
            PERFORM DUMP-BYTE
            ADD 1 TO THE-BYTE
            END-PERFORM.
        GOBACK.
        DUMP-BYTE.
            MOVE ZERO TO DUMPER1
            MOVE BYTE-TO-DUMP TO DUMPER2
            DIVIDE DUMPER1 BY 16 GIVING DUMPER1
            ADD 1 TO DUMPER1
            DISPLAY HEXCHAR(DUMPER1) NO ADVANCING.
            MOVE ZERO TO DUMPER1
            MOVE BYTE-TO-DUMP TO DUMPER2
            MOVE FUNCTION MOD(DUMPER1 16) TO DUMPER1
            ADD 1 TO DUMPER1
            IF LADVANCE EQUAL TO 1 THEN
                DISPLAY HEXCHAR(DUMPER1)
            ELSE
                DISPLAY HEXCHAR(DUMPER1) NO ADVANCING
            END-IF.
        END PROGRAM dump.
])
AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([./a.out], [0],
[1f202020202020202020
012f2020202020202020
123f2020202020202020
01234f20202020202020
12345f20202020202020
0123456f202020202020
1234567f202020202020
012345678f2020202020
123456789f2020202020
01234567890f20202020
12345678901f20202020
0123456789012f202020
1234567890123f202020
012345678901234f2020
123456789012345f2020
01234567890123456f20
12345678901234567f20
0123456789012345678f
1d202020202020202020
012d2020202020202020
123d2020202020202020
01234d20202020202020
12345d20202020202020
0123456d202020202020
1234567d202020202020
012345678d2020202020
123456789d2020202020
01234567890d20202020
12345678901d20202020
0123456789012d202020
1234567890123d202020
012345678901234d2020
123456789012345d2020
01234567890123456d20
12345678901234567d20
0123456789012345678d
0f202020202020202020
000f2020202020202020
000f2020202020202020
00000f20202020202020
00000f20202020202020
0000000f202020202020
0000000f202020202020
000000000f2020202020
000000000f2020202020
00000000000f20202020
00000000000f20202020
0000000000000f202020
0000000000000f202020
000000000000000f2020
000000000000000f2020
00000000000000000f20
00000000000000000f20
0000000000000000000f
0c202020202020202020
000c2020202020202020
000c2020202020202020
00000c20202020202020
00000c20202020202020
0000000c202020202020
0000000c202020202020
000000000c2020202020
000000000c2020202020
00000000000c20202020
00000000000c20202020
0000000000000c202020
0000000000000c202020
000000000000000c2020
000000000000000c2020
00000000000000000c20
00000000000000000c20
0000000000000000000c
0f202020202020202020
000f2020202020202020
000f2020202020202020
00000f20202020202020
00000f20202020202020
0000000f202020202020
0000000f202020202020
000000000f2020202020
000000000f2020202020
00000000000f20202020
00000000000f20202020
0000000000000f202020
0000000000000f202020
000000000000000f2020
000000000000000f2020
00000000000000000f20
00000000000000000f20
0000000000000000000f
0c202020202020202020
000c2020202020202020
000c2020202020202020
00000c20202020202020
00000c20202020202020
0000000c202020202020
0000000c202020202020
000000000c2020202020
000000000c2020202020
00000000000c20202020
00000000000c20202020
0000000000000c202020
0000000000000c202020
000000000000000c2020
000000000000000c2020
00000000000000000c20
00000000000000000c20
0000000000000000000c
])
AT_CLEANUP


AT_SETUP([PACKED-DECIMAL used with DISPLAY])
AT_KEYWORDS([packed])
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 X-99          PIC 99   USAGE PACKED-DECIMAL.
       01 X-S99         PIC S99  USAGE PACKED-DECIMAL.
       01 X-999         PIC 999  USAGE PACKED-DECIMAL.
       01 X-S999        PIC S999 USAGE PACKED-DECIMAL.
       PROCEDURE        DIVISION.
           MOVE    0 TO X-99.
           DISPLAY X-99
           END-DISPLAY.
           MOVE   99 TO X-99.
           DISPLAY X-99
           END-DISPLAY.
           MOVE    0 TO X-S99.
           DISPLAY X-S99
           END-DISPLAY.
           MOVE   -1 TO X-S99.
           DISPLAY X-S99
           END-DISPLAY.
           MOVE    0 TO X-999.
           DISPLAY X-999
           END-DISPLAY.
           MOVE  123 TO X-999.
           DISPLAY X-999
           END-DISPLAY.
           MOVE    0 TO X-S999.
           DISPLAY X-S999
           END-DISPLAY.
           MOVE -123 TO X-S999.
           DISPLAY X-S999
           END-DISPLAY.
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([./a.out], [0],
[00
99
+00
-01
000
123
+000
-123
])

AT_CLEANUP


AT_SETUP([PACKED-DECIMAL used with MOVE])
AT_KEYWORDS([packed])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 X-99          PIC 99   USAGE PACKED-DECIMAL.
       01 X-S99         PIC S99  USAGE PACKED-DECIMAL.
       01 X-999         PIC 999  USAGE PACKED-DECIMAL.
       01 X-S999        PIC S999 USAGE PACKED-DECIMAL.
       01 C-P1234       PIC 9999  VALUE 1234.
       01 C-N1234       PIC S9999 VALUE -1234.
       PROCEDURE        DIVISION.
           MOVE C-P1234 TO X-99.
           DISPLAY X-99
           END-DISPLAY.
           MOVE C-P1234 TO X-S99.
           DISPLAY X-S99
           END-DISPLAY.
           MOVE C-P1234 TO X-999.
           DISPLAY X-999
           END-DISPLAY.
           MOVE C-P1234 TO X-S999.
           DISPLAY X-S999
           END-DISPLAY.
           MOVE C-N1234 TO X-99.
           DISPLAY X-99
           END-DISPLAY.
           MOVE C-N1234 TO X-S99.
           DISPLAY X-S99
           END-DISPLAY.
           MOVE C-N1234 TO X-999.
           DISPLAY X-999
           END-DISPLAY.
           MOVE C-N1234 TO X-S999.
           DISPLAY X-S999
           END-DISPLAY.
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([./a.out], [0],
[34
+34
234
+234
34
-34
234
-234
])

AT_CLEANUP


AT_SETUP([PACKED-DECIMAL used with INITIALIZE])
AT_KEYWORDS([packed])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 X-99          PIC 99   USAGE PACKED-DECIMAL.
       01 X-S99         PIC S99  USAGE PACKED-DECIMAL.
       01 X-999         PIC 999  USAGE PACKED-DECIMAL.
       01 X-S999        PIC S999 USAGE PACKED-DECIMAL.
       PROCEDURE        DIVISION.
           INITIALIZE X-99.
           DISPLAY X-99
           END-DISPLAY.
           INITIALIZE X-S99.
           DISPLAY X-S99
           END-DISPLAY.
           INITIALIZE X-999.
           DISPLAY X-999
           END-DISPLAY.
           INITIALIZE X-S999.
           DISPLAY X-S999
           END-DISPLAY.
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([./a.out], [0],
[00
+00
000
+000
])

AT_CLEANUP


AT_SETUP([PACKED-DECIMAL arithmetic])
AT_KEYWORDS([packed])

AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 X             PIC 99 USAGE PACKED-DECIMAL VALUE 0.
       01 Y             PIC 99 USAGE PACKED-DECIMAL VALUE 9.
       PROCEDURE        DIVISION.
           COMPUTE X = 1
           END-COMPUTE.
           DISPLAY X
           END-DISPLAY.
           COMPUTE X = Y
           END-COMPUTE.
           DISPLAY X
           END-DISPLAY.
           COMPUTE X = X + Y
           END-COMPUTE.
           DISPLAY X
           END-DISPLAY.
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([./a.out], [0],
[01
09
18
])
AT_CLEANUP

AT_SETUP([PACKED-DECIMAL numeric test (1)])
AT_KEYWORDS([packed])
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 G.
         02 X-2         PIC X(2).
         02 N-2         REDEFINES X-2 PIC 999  USAGE PACKED-DECIMAL.
         02 N-S2        REDEFINES X-2 PIC S999 USAGE PACKED-DECIMAL.
       PROCEDURE        DIVISION.
           MOVE X"0000" TO X-2.
           IF N-2  IS NUMERIC
            DISPLAY "1 NG"
            END-DISPLAY
           ELSE
            DISPLAY "OK"
            END-DISPLAY
           END-IF.
           IF N-S2 IS NUMERIC
            DISPLAY "2 NG"
            END-DISPLAY
           ELSE
            DISPLAY "OK"
            END-DISPLAY
           END-IF.
           MOVE X"000c" TO X-2.
           IF N-2  IS NUMERIC
            DISPLAY "3 NG"
            END-DISPLAY
           ELSE
            DISPLAY "OK"
            END-DISPLAY
           END-IF.
           IF N-S2 IS NUMERIC
            DISPLAY "OK"
            END-DISPLAY
           ELSE
            DISPLAY "4 NG"
            END-DISPLAY
           END-IF.
           MOVE X"000d" TO X-2.
           IF N-2  IS NUMERIC
            DISPLAY "5 NG"
            END-DISPLAY
           ELSE
            DISPLAY "OK"
            END-DISPLAY
           END-IF.
           IF N-S2 IS NUMERIC
            DISPLAY "OK"
            END-DISPLAY
           ELSE
            DISPLAY "6 NG"
            END-DISPLAY
           END-IF.
           MOVE X"000f" TO X-2.
           IF N-2  IS NUMERIC
            DISPLAY "OK"
            END-DISPLAY
           ELSE
            DISPLAY "7 NG"
            END-DISPLAY
           END-IF.
           IF N-S2 IS NUMERIC
            DISPLAY "8 NG"
            END-DISPLAY
           ELSE
            DISPLAY "OK"
            END-DISPLAY
           END-IF.
           MOVE X"1234" TO X-2.
           IF N-2  IS NUMERIC
            DISPLAY "9 NG"
            END-DISPLAY
           ELSE
            DISPLAY "OK"
            END-DISPLAY
           END-IF.
           IF N-S2 IS NUMERIC
            DISPLAY "10 NG"
            END-DISPLAY
           ELSE
            DISPLAY "OK"
            END-DISPLAY
           END-IF.
           MOVE X"999f" TO X-2.
           IF N-2  IS NUMERIC
            DISPLAY "OK"
            END-DISPLAY
           ELSE
            DISPLAY "11 NG"
            END-DISPLAY
           END-IF.
           IF N-S2 IS NUMERIC
            DISPLAY "12 NG"
            END-DISPLAY
           ELSE
            DISPLAY "OK"
            END-DISPLAY
           END-IF.
           MOVE X"ffff" TO X-2.
           IF N-2  IS NUMERIC
            DISPLAY "13 NG"
            END-DISPLAY
           ELSE
            DISPLAY "OK"
            END-DISPLAY
           END-IF.
           IF N-S2 IS NUMERIC
            DISPLAY "14 NG"
            END-DISPLAY
           ELSE
            DISPLAY "OK"
            END-DISPLAY
           END-IF.
           STOP RUN.
])
AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([./a.out], [0],
[OK
OK
OK
OK
OK
OK
OK
OK
OK
OK
OK
OK
OK
OK
])
AT_CLEANUP

AT_SETUP([PACKED-DECIMAL numeric test (2)])
AT_KEYWORDS([packed])
# HEX literals not yet supported TODO
# Intrinsic Function NUMERIC not yet supported TODO
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 G.
         02 X-2         PIC X(2).
         02 N-2         REDEFINES X-2 PIC 999  USAGE PACKED-DECIMAL.
         02 N-S2        REDEFINES X-2 PIC S999 USAGE PACKED-DECIMAL.
       PROCEDURE        DIVISION.
           MOVE X"0000" TO X-2.
           IF N-2  IS NUMERIC
            DISPLAY "NG 1"
           ELSE
            DISPLAY "OK"
           END-IF.
           IF N-S2 IS NUMERIC
            DISPLAY "NG 2"
           ELSE
            DISPLAY "OK"
           END-IF.
           MOVE X"000c" TO X-2.
           IF N-2  IS NUMERIC
            DISPLAY "NG 3"
           ELSE
            DISPLAY "OK"
           END-IF.
           IF N-S2 IS NUMERIC
            DISPLAY "OK"
           ELSE
            DISPLAY "NG 4"
           END-IF.
           MOVE X"000d" TO X-2.
           IF N-2  IS NUMERIC
            DISPLAY "NG 5"
           ELSE
            DISPLAY "OK"
           END-IF.
           IF N-S2 IS NUMERIC
            DISPLAY "OK"
           ELSE
            DISPLAY "NG 6"
           END-IF.
           MOVE X"000f" TO X-2.
           IF N-2  IS NUMERIC
            DISPLAY "OK"
           ELSE
            DISPLAY "NG 7"
           END-IF.
           IF N-S2 IS NUMERIC
            DISPLAY "NG 8"
           ELSE
            DISPLAY "OK"
           END-IF.
           MOVE X"1234" TO X-2.
           IF N-2  IS NUMERIC
            DISPLAY "NG 9"
           ELSE
            DISPLAY "OK"
           END-IF.
           IF N-S2 IS NUMERIC
            DISPLAY "NG 10"
           ELSE
            DISPLAY "OK"
           END-IF.
           MOVE X"999f" TO X-2.
           IF N-2  IS NUMERIC
            DISPLAY "OK"
           ELSE
            DISPLAY "NG 11"
           END-IF.
           IF N-S2 IS NUMERIC
            DISPLAY "NG 12"
           ELSE
            DISPLAY "OK"
           END-IF.
           MOVE X"ffff" TO X-2.
           IF N-2  IS NUMERIC
            DISPLAY "NG 13"
           ELSE
            DISPLAY "OK"
           END-IF.
           IF N-S2 IS NUMERIC
            DISPLAY "NG 14"
           ELSE
            DISPLAY "OK"
           END-IF.
           STOP RUN.
])

AT_CHECK([$COMPILE prog.cob], [0], [], [])
AT_CHECK([./a.out], [0],
[OK
OK
OK
OK
OK
OK
OK
OK
OK
OK
OK
OK
OK
OK
])
AT_CLEANUP

AT_SETUP([PACKED-DECIMAL basic comp-3/comp-6 (1)])
AT_KEYWORDS([packed comp-6])
AT_DATA([prog.cob], [
        IDENTIFICATION DIVISION.
        PROGRAM-ID. prog.
        DATA DIVISION.
        WORKING-STORAGE SECTION.
        01  x1 PIC 9 COMP-3.
        01  x2 PIC 99 COMP-3.
        01  x3 PIC 999 COMP-3.
        01  x4 PIC 9999 COMP-3.
        01  x5 PIC 99999 COMP-3.
        01  x6 PIC 999999 COMP-3.
        01  y1 PIC 9 COMP-6.
        01  y2 PIC 99 COMP-6.
        01  y3 PIC 999 COMP-6.
        01  y4 PIC 9999 COMP-6.
        01  y5 PIC 99999 COMP-6.
        01  y6 PIC 999999 COMP-6.
        procedure division.
        display "check lengths of comp-3"
        display FUNCTION LENGTH(x1) " should be 1"
        display FUNCTION LENGTH(x2) " should be 2"
        display FUNCTION LENGTH(x3) " should be 2"
        display FUNCTION LENGTH(x4) " should be 3"
        display FUNCTION LENGTH(x5) " should be 3"
        display FUNCTION LENGTH(x6) " should be 4"
        display "check lengths of comp-6"       
        display FUNCTION LENGTH(y1) " should be 1"
        display FUNCTION LENGTH(y2) " should be 1"
        display FUNCTION LENGTH(y3) " should be 2"
        display FUNCTION LENGTH(y4) " should be 2"
        display FUNCTION LENGTH(y5) " should be 3"
        display FUNCTION LENGTH(y6) " should be 3"
        move 654321 to x1 x2 x3 x4 x5 x6 y1 y2 y3 y4 y5 y6
        display "results of MOVE TO COMP-3"
        display x1
        display x2
        display x3
        display x4
        display x5
        display x6
        display "results of MOVE TO COMP-6"
        display y1
        display y2
        display y3
        display y4
        display y5
        display y6
        goback.
])
AT_CHECK([$COMPILE -dialect mf prog.cob], [0], [], [])
AT_CHECK([./a.out], [0], [check lengths of comp-3
1 should be 1
2 should be 2
2 should be 2
3 should be 3
3 should be 3
4 should be 4
check lengths of comp-6
1 should be 1
1 should be 1
2 should be 2
2 should be 2
3 should be 3
3 should be 3
results of MOVE TO COMP-3
1
21
321
4321
54321
654321
results of MOVE TO COMP-6
1
21
321
4321
54321
654321
], [])
AT_CLEANUP

AT_SETUP([PACKED-DECIMAL basic comp-3/comp-6 (2)])
AT_KEYWORDS([packed comp-6])
AT_DATA([prog.cob], [
        identification      division.
        program-id.         prog.
        data                division.
        working-storage     section.
        01  vars.
         05 var1d                                                  .
          10 var01      pic  99v99     comp-3       value  43.21 .
          10 filler                     binary-double value zero   . 
         05 var1 redefines var1d        pointer                    .
         05 var2d                                                  .
          10 var02      pic s99v99     comp-3       value  43.21 .
          10 filler                     binary-double value zero   . 
         05 var2 redefines var2d        pointer                    .
         05 var3d                                                  .
          10 var03      pic s99v99     comp-3       value -43.21 .
          10 filler                     binary-double value zero   . 
         05 var3 redefines var3d        pointer                    .
         05 var4d                                                  .
          10 var04      pic  99v99     comp-6       value  43.21 .
          10 filler                     binary-double value zero   . 
         05 var4 redefines var4d        pointer                    .
        procedure           division.
        display length of var01 space var1 space space var01
        display length of var02 space var2 space var02
        display length of var03 space var3 space var03
        display length of var04 space var4 space space var04
        move 12.34 to var01
        move 12.34 to var02
        move 12.34 to var03
        move 12.34 to var04
        display function length(var01) space var1 space space var01
        display function length(var02) space var2 space var02
        display function length(var03) space var3 space var03
        display function length(var04) space var4 space space var04
        goback.
        end program         prog.
])
AT_CHECK([$COMPILE -dialect mf prog.cob], [0], [], [])
AT_CHECK([./a.out], [0], [3 0x00000000001f3204  43.21
3 0x00000000001c3204 +43.21
3 0x00000000001d3204 -43.21
2 0x0000000000002143  43.21
3 0x00000000004f2301  12.34
3 0x00000000004c2301 +12.34
3 0x00000000004c2301 +12.34
2 0x0000000000003412  12.34
], [])
AT_CLEANUP

AT_SETUP([COMP-6 used with DISPLAY])
AT_KEYWORDS([packed])
AT_SKIP_IF(false)
# COMP-6 not ISO-IEC 2014 - SIGNED version of PACKED-DECIMAL(?)
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 X-99          PIC 99   USAGE COMP-6.
       01 X-999         PIC 999  USAGE COMP-6.
       PROCEDURE        DIVISION.
           MOVE    0 TO X-99.
           DISPLAY X-99
           END-DISPLAY.
           MOVE   99 TO X-99.
           DISPLAY X-99
           END-DISPLAY.
           MOVE    0 TO X-999.
           DISPLAY X-999
           END-DISPLAY.
           MOVE  123 TO X-999.
           DISPLAY X-999
           END-DISPLAY.
           STOP RUN.
])
AT_CHECK([$COMPILE -dialect mf prog.cob], [0], [], [])
AT_CHECK([./a.out], [0],
[00
99
000
123
])
AT_CLEANUP

AT_SETUP([COMP-6 used with MOVE])
AT_KEYWORDS([packed])
AT_SKIP_IF(false)
# COMP-6 not ISO-IEC 2014 - UNSIGNED version of PACKED-DECIMAL
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 X-99          PIC 99   USAGE COMP-6.
       01 X-999         PIC 999  USAGE COMP-6.
       01 B-99          USAGE BINARY-LONG.
       01 B-999         USAGE BINARY-LONG.
       PROCEDURE        DIVISION.
           MOVE    0 TO B-99.
           MOVE B-99 TO X-99.
           DISPLAY X-99
           END-DISPLAY.
           MOVE   99 TO B-99.
           MOVE B-99 TO X-99.
           DISPLAY X-99
           END-DISPLAY.
           MOVE    0  TO B-999.
           MOVE B-999 TO X-999.
           DISPLAY X-999
           END-DISPLAY.
           MOVE  123  TO B-999.
           MOVE B-999 TO X-999.
           DISPLAY X-999
           END-DISPLAY.
           MOVE B-999 TO X-99.
           DISPLAY X-99
           END-DISPLAY.
           STOP RUN.
])
AT_CHECK([$COMPILE -dialect mf prog.cob], [0], [], [])
AT_CHECK([./a.out], [0],
[00
99
000
123
23
])
AT_CLEANUP


AT_SETUP([COMP-6 arithmetic])
AT_KEYWORDS([packed])
AT_SKIP_IF(false)
# COMP-6 not ISO-IEC 2014 - Modified to PACKED-DECIMAL
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 X-99          PIC 99   USAGE COMP-6.
       01 X-999         PIC 999  USAGE COMP-6.
       01 B-99          USAGE BINARY-LONG UNSIGNED.
       01 B-999         USAGE BINARY-LONG UNSIGNED.
       PROCEDURE        DIVISION.
           MOVE   99  TO B-99
           MOVE B-99  TO X-99
           MOVE  123  TO B-999
           MOVE B-999 TO X-999
           ADD  X-99  X-999 GIVING B-99
           END-ADD
           DISPLAY B-99
           END-DISPLAY
           STOP RUN.
])
AT_CHECK([$COMPILE -dialect mf prog.cob], [0], [], [])
AT_CHECK([./a.out], [0],
[0000000222
])
AT_CLEANUP


AT_SETUP([COMP-6 numeric test])
AT_KEYWORDS([packed])
AT_SKIP_IF(false)
# COMP-6 not ISO-IEC 2014 - DIALECT TODO
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 G.
          02 X-2         PIC X(2).
          02 N-3         REDEFINES X-2 PIC 999  USAGE COMP-6.
          02 N-4         REDEFINES X-2 PIC 9999 USAGE COMP-6.
       PROCEDURE        DIVISION.
           MOVE X"0000" TO X-2.
           IF N-3  IS NUMERIC
            DISPLAY "OK"
            END-DISPLAY
           ELSE
            DISPLAY "1 NG"
            END-DISPLAY
           END-IF.
           IF N-4  IS NUMERIC
            DISPLAY "OK"
            END-DISPLAY
           ELSE
            DISPLAY "2 NG"
            END-DISPLAY
           END-IF.
           MOVE X"000c" TO X-2.
           IF N-3  IS NUMERIC
            DISPLAY "3 NG"
            END-DISPLAY
           ELSE
            DISPLAY "OK"
            END-DISPLAY
           END-IF.
           IF N-4  IS NUMERIC
            DISPLAY "4 NG"
            END-DISPLAY
           ELSE
            DISPLAY "OK"
            END-DISPLAY
           END-IF.
           MOVE X"1234" TO X-2.
           IF N-3  IS NUMERIC
            DISPLAY "5 NG"
            END-DISPLAY
           ELSE
            DISPLAY "OK"
            END-DISPLAY
           END-IF.
           IF N-4  IS NUMERIC
            DISPLAY "OK"
            END-DISPLAY
           ELSE
            DISPLAY "6 NG"
            END-DISPLAY
           END-IF.
           MOVE X"ffff" TO X-2.
           IF N-3  IS NUMERIC
            DISPLAY "7 NG"
            END-DISPLAY
           ELSE
            DISPLAY "OK"
            END-DISPLAY
           END-IF.
           IF N-4  IS NUMERIC
            DISPLAY "7 NG"
            END-DISPLAY
           ELSE
            DISPLAY "OK"
            END-DISPLAY
           END-IF.
           STOP RUN.
])
AT_CHECK([$COMPILE -dialect mf prog.cob], [0], [], [])
AT_CHECK([./a.out], [0],
[OK
OK
OK
OK
OK
OK
OK
OK
])
AT_CLEANUP

