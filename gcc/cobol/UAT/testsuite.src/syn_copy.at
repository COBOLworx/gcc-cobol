## Copyright (C) 2003-2012, 2014-2015, 2017-2018, 2020 Free Software Foundation, Inc.
## Written by Keisuke Nishida, Roger While, Simon Sobisch
##
## This file is part of GnuCOBOL.
##
## The GnuCOBOL compiler is free software: you can redistribute it
## and/or modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## GnuCOBOL is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with GnuCOBOL.  If not, see <https://www.gnu.org/licenses/>.

### GnuCOBOL Test Suite
#############################################################
### COBOL for GCC Test Suite - testsuite adapted by Marty Heyman
## Copyright (C) 2022-23 COBOLworx, a subsidiary of Symas Corp.

AT_SETUP([COPY: within comment])
AT_KEYWORDS([copy])
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
      *>COPY "copy.inc".
       PROCEDURE        DIVISION.
           STOP RUN.
])

AT_DATA([prog2.cob], [
  IDENTIFICATION   DIVISION.
  PROGRAM-ID.      prog2.
  DATA             DIVISION.
  WORKING-STORAGE  SECTION.
  *> COPY "copy.inc".
  PROCEDURE        DIVISION.
  STOP RUN.
])
AT_CHECK([$COMPILE_ONLY prog.cob], [0], [], [])
AT_CHECK([$COMPILE_ONLY -free prog2.cob], [0], [], [])
AT_CLEANUP


AT_SETUP([COPY: separators])
AT_KEYWORDS([copy])
# TODO - is the second REPLACING supposed to be ignored when the
#   second one can't find TEST-VAR any more?
AT_DATA([copy.inc], [
       01 TEST-VAR PIC X(2) VALUE "OK".
])
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       COPY "copy.inc"
          REPLACING ==TEST-VAR==, BY ==FIRST-MATCH==,
                 ,  ==TEST-VAR==; BY ==SECOND-MATCH==;
                 ;  ==TEST-VAR== , BY ==THIRD-MATCH==
                    ==TEST-VAR== ; BY ==FOURTH-MATCH==.
       PROCEDURE        DIVISION.
           DISPLAY FIRST-MATCH NO ADVANCING
           END-DISPLAY.
           STOP RUN.
])
AT_CHECK([$COMPILE -I. prog.cob], [0], [], [])
AT_CHECK([./a.out], [0], [OK], [])
AT_CLEANUP

AT_SETUP([COPY: IN / OF / -I])
AT_KEYWORDS([copy])
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       COPY "copy.inc" IN SUB.
       PROCEDURE        DIVISION.
           DISPLAY TEST-VAR.
           STOP RUN.
])
AT_CHECK([mkdir -p SUB], [0], [], [])
AT_DATA([SUB/copy.inc], [
       77  TEST-VAR     PIC X VALUE '1'.
])
AT_CHECK([$COMPILE_ONLY prog.cob], [0], [], [])
AT_CLEANUP

AT_SETUP([COPY filename with .cpy extension])
# Not implementing literal-1
AT_KEYWORDS([v1-bugs fixed-format])
AT_CHECK([mkdir cpy], [0], [], [])
AT_DATA([cpy/foo.cpy], [
       DISPLAY "OK".
])
AT_DATA([prog.cob], [
       IDENTIFICATION DIVISION.
       PROGRAM-ID. prog.
       PROCEDURE DIVISION.
      *> remove the following ".cpy" to make the program compile and run.
       COPY foo.cpy.
       EXIT PROGRAM.
       END PROGRAM prog.
])
AT_CHECK([$COMPILE -Icpy -o prog prog.cob], [0], [], [])
AT_CHECK([./prog], [0], [OK
], [])
AT_CLEANUP

AT_SETUP([COPY filename with no .cpy extension])
# Not implementing literal-1
# We have assumed suffixes we look for. Suffixes are optional.
AT_KEYWORDS([v1-bugs fixed-format])
AT_CHECK([mkdir cpy], [0], [], [])
AT_DATA([cpy/foo], [
       DISPLAY "OK".
])
AT_DATA([prog.cob], [
       IDENTIFICATION DIVISION.
       PROGRAM-ID. prog.
       PROCEDURE DIVISION.
       COPY foo.
       EXIT PROGRAM.
       END PROGRAM prog.
])
AT_CHECK([$COMPILE -Icpy -o prog prog.cob], [0], [], [])
AT_CHECK([./prog], [0], [OK
], [])
AT_CLEANUP

########
# Deferring "COPY foo in sub" where "-I." is on the GCOBOL command line
#    it's the "IN SUB" part that's likely not implemented

AT_SETUP([COPY: replacement order - 1])
AT_KEYWORDS([former-bugs copy])
AT_DATA([xxxx.cpy], [
       01 TEST-VAR PIC X(2) VALUE "OK".
])
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       COPY xxxx
          REPLACING ==TEST-VAR== BY ==FIRST-MATCH==
                    ==TEST-VAR== BY ==SECOND-MATCH==.
       PROCEDURE        DIVISION.
           DISPLAY FIRST-MATCH NO ADVANCING
           END-DISPLAY.
           STOP RUN.
])
AT_CHECK([$COMPILE -I. prog.cob], [0], [], [])
# AT_CHECK([$COMPILE_ONLY prog.cob], [0], [], [])
AT_CHECK([./a.out], [0], [OK], [])
AT_CLEANUP

AT_SETUP([COPY: replacement order - 2])
# Sub-test without multiple REPLACING clauses
# REPLACING works. With single REPLACING clause.
AT_KEYWORDS([former-bugs copy])
AT_DATA([xxxx.cpy], [
       01 TEST-VAR PIC X(2) VALUE "OK".
])
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       COPY xxxx
          REPLACING ==TEST-VAR== BY ==FIRST-MATCH==.
       PROCEDURE        DIVISION.
           DISPLAY FIRST-MATCH NO ADVANCING
           END-DISPLAY.
           STOP RUN.
])
AT_CHECK([$COMPILE -I. prog.cob], [0], [], [])
# AT_CHECK([$COMPILE_ONLY prog.cob], [0], [], [])
AT_CHECK([./a.out], [0], [OK], [])
AT_CLEANUP

AT_SETUP([COPY: replacement order - 3])
# Sub-test without multiple REPLACING clauses
# REPLACING works. With two REPLACING clauses but different
#   REPLACING targets..
# The REPLACING keyword appears at most once in COPY ... REPLACING.
# Pseudotext elements are, per ISO and IBM, "COBOL words", not mere strings.
# Thus ==OK== BY ==HI== does NOT replace "OK".
AT_KEYWORDS([v1-bugs copy])
AT_DATA([xxxx.cpy], [
       01 TEST-VAR PIC X(2) VALUE "OK".
])
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       COPY xxxx
          REPLACING =="OK"==     BY =="HI"==
                    ==TEST-VAR== BY ==FIRST-MATCH==.
       PROCEDURE        DIVISION.
           DISPLAY FIRST-MATCH NO ADVANCING
           END-DISPLAY.
           STOP RUN.
])
AT_CHECK([$COMPILE -I. prog.cob], [0], [], [])
# AT_CHECK([$COMPILE_ONLY prog.cob], [0], [], [])
AT_CHECK([./a.out], [0], [HI], [])
AT_CLEANUP


AT_SETUP([COPY: IN / OF / -I: 2])
AT_KEYWORDS([copy cobc])
AT_DATA([prog2.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       COPY "copy.inc" OF SUB.
       PROCEDURE        DIVISION.
           DISPLAY TEST-VAR.
           STOP RUN.
])
AT_CHECK([mkdir -p sub2], [0], [], [])
AT_DATA([sub2/copy.inc], [
       77  TEST-VAR     PIC X VALUE '2'.
])
# Use SUB environment variable to define sub2 directory name for library.
AT_CHECK([SUB=sub2 $COMPILE_ONLY prog2.cob], [0], [], [])
AT_CLEANUP

AT_SETUP([393 COPY: IN / OF / -I: 4])
AT_KEYWORDS([copy cobc])
AT_DATA([prog4.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       COPY "copy.inc".
       PROCEDURE        DIVISION.
           DISPLAY TEST-VAR.
           STOP RUN.
])
AT_CHECK([mkdir -p SUB], [0], [], [])
AT_DATA([SUB/copy.inc], [
       77  TEST-VAR     PIC X VALUE '1'.
])
AT_CHECK([$COMPILE_ONLY prog4.cob -I SUB], [0], [], [])
AT_CLEANUP

AT_SETUP([COPY: recursive])
AT_KEYWORDS([copy])
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       COPY copy1.
       PROCEDURE        DIVISION.
           DISPLAY TEST-VAR.
           STOP RUN.
])
AT_DATA([copy1.CPY], [
       COPY copy2.
       01 TEST-VAR  PIC X(2) VALUE "V1".
])
AT_DATA([copy2.CPY], [
       01 TEST-VAR2 PIC X(2) VALUE "V2".
       COPY copy3.
])
AT_DATA([copy3.CPY],
[       COPY "copy1.CPY".
       01 TEST-VAR3 PIC X(2) VALUE "V3".
])
AT_CHECK([$COMPILE_ONLY prog.cob], [1], [], [In file included from copy3.CPY:128,
                 from copy2.CPY:128,
                 from copy1.CPY:128,
                 from prog.cob:128:
copy1.CPY:2:1: error: recursive copybook: 'copy1.CPY' includes itself
    2 |        COPY copy2.
      | ^
cobol1: error: failed compiling prog.cob
])
AT_CLEANUP


