## Copyright (C) 2003-2012, 2014-2015, 2017-2018, 2020 Free Software Foundation, Inc.
## Written by Keisuke Nishida, Roger While, Simon Sobisch
##
## This file is part of GnuCOBOL.
##
## The GnuCOBOL compiler is free software: you can redistribute it
## and/or modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## GnuCOBOL is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with GnuCOBOL.  If not, see <https://www.gnu.org/licenses/>.

### GnuCOBOL Test Suite

### ISO+IEC+1989-2002 8.4.2.3 Reference-modification

## 8.4.2.3.3 General rules

AT_SETUP([valid reference modification])
AT_KEYWORDS([refmod])
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 X             PIC X(4) VALUE "abcd".
       PROCEDURE        DIVISION.
           DISPLAY X(1:1) ":" X(1:2) ":" X(1:3) ":" X(1:4) ":" X(1:)
           END-DISPLAY.
           DISPLAY X(2:1) ":" X(2:2) ":" X(2:3) ":" X(2:)
           END-DISPLAY.
           DISPLAY X(3:1) ":" X(3:2) ":" X(3:)
           END-DISPLAY.
           DISPLAY X(4:1) ":" X(4:)
           END-DISPLAY.
           STOP RUN.
])
AT_CHECK([$COMPILE prog.cob], [0], [], [])
# mheyman 2023-1-4 Add positive execution test
AT_CHECK([./a.out], [0], [a:ab:abc:abcd:abcd
b:bc:bcd:bcd
c:cd:cd
d:d
], [])
AT_CLEANUP

AT_SETUP([Static out of bounds])
AT_KEYWORDS([refmod])
AT_DATA([prog.cob], [
       IDENTIFICATION   DIVISION.
       PROGRAM-ID.      prog.
       DATA             DIVISION.
       WORKING-STORAGE  SECTION.
       01 X             PIC X(4).
       01 Y             PIC 9 VALUE 1.
       PROCEDURE        DIVISION.
           DISPLAY X(0:1)
           END-DISPLAY.
           DISPLAY X(0:Y)
           END-DISPLAY.
           DISPLAY X(5:1)
           END-DISPLAY.
           DISPLAY X(5:Y)
           END-DISPLAY.
           DISPLAY X(1:0)
           END-DISPLAY.
           DISPLAY X(Y:0)
           END-DISPLAY.
           DISPLAY X(1:5)
           END-DISPLAY.
           DISPLAY X(Y:5)
           END-DISPLAY.
           DISPLAY X(3:3)
           END-DISPLAY.
           STOP RUN.
])
AT_CHECK([$COMPILE_ONLY prog.cob], [1], [],[prog.cob:9:21: error: X(0) out of bounds, size is 4
    9 |            DISPLAY X(0:1)
      |                     ^
prog.cob:11:21: error: X(0) out of bounds, size is 4
   11 |            DISPLAY X(0:Y)
      |                     ^
prog.cob:13:21: error: X(5) out of bounds, size is 4
   13 |            DISPLAY X(5:1)
      |                     ^
prog.cob:15:21: error: X(5) out of bounds, size is 4
   15 |            DISPLAY X(5:Y)
      |                     ^
prog.cob:6:8: error: X(1:0) out of bounds, size is 4
    6 |        01 X             PIC X(4).
      |        ^
prog.cob:19:21: error: X(Y:0) out of bounds, size is 4
   19 |            DISPLAY X(Y:0)
      |                     ^
prog.cob:6:8: error: X(1:5) out of bounds, size is 4
    6 |        01 X             PIC X(4).
      |        ^
prog.cob:23:21: error: X(Y:5) out of bounds, size is 4
   23 |            DISPLAY X(Y:5)
      |                     ^
prog.cob:6:8: error: X(3:3) out of bounds, size is 4
    6 |        01 X             PIC X(4).
      |        ^
cobol1: error: failed compiling prog.cob
])
AT_CLEANUP

