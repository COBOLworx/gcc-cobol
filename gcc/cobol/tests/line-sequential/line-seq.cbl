        IDENTIFICATION DIVISION.
        PROGRAM-ID.  line-seq.

        ENVIRONMENT DIVISION.
        INPUT-OUTPUT SECTION.
        FILE-CONTROL.
            SELECT  DATA-FILE
            ASSIGN TO
            "data.tab" ORGANIZATION LINE SEQUENTIAL.

        DATA DIVISION.

        FILE SECTION.
        FD  DATA-FILE.
        01  DATA-RECORD                  PIC X(80).

        WORKING-STORAGE SECTION.
        01 RECORD-COUNT PIC 99 VALUE ZERO.

        PROCEDURE DIVISION.
        OPEN    INPUT   DATA-FILE.
        READ-LOOP.
        READ    DATA-FILE
                AT END
                DISPLAY "We saw " RECORD-COUNT " records; there should"
                        " have been 09"
                CLOSE DATA-FILE
                STOP RUN.
        ADD 1 to RECORD-COUNT
        GO TO READ-LOOP.
        END PROGRAM line-seq.
